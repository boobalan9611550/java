package exception;

import java.util.Scanner;

public class excetionhandling {
	public static void main(String[] args) {

		excetionhandling ed = new excetionhandling();
		Scanner kv = new Scanner(System.in);
		System.out.println("Enter no1 :");
		int no1 = kv.nextInt();
		System.out.println("Enter no2 :");
		int no2 = kv.nextInt();

		ed.divide(no1, no2);
		ed.add(no1, no2);
	 }

	private void add(int no1, int no2) {
		System.out.println(no1 + no2);
	}

	private void divide(int no1, int no2) {
		try {
			System.out.println(no1 / no2);
		} catch (ArithmeticException ae) {
			ae.printStackTrace();
		}
  }
  }
