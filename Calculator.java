public class Calculator
{
public static void main(String[] args)
{ 
		Calculator vv = new Calculator();
		float fl = vv.add();
	    vv.divide(fl);  //parameter    //method calling statement
	    vv.divide(fl,20);
	    vv.divide("vishnu");
		System.out.println(vv.add()); 
	// method overloading or complinetime polymorphism
	// same method with different no.of agruments or 
	// with different types of arguments
}
public float add() //return  type required
{
    return 200+400.5f; //400.5 float
}	
public void divide (float re) //arguments
{   // method definition
	System.out.println(re/20);
}
public void divide (float re,int no)
	{
		System.out.println(re/no);//400.5/10
	}
public void divide(String msg)	
{
	System.out.println(msg);
}
}
