public class Bookshop
{
	String auther;
	int price;
	int pages;
	public Bookshop(String s1,int i,int j)
	{
		auther=s1;
	    price=i;
	    pages=j;
}
	public static void main(String[] args)   
	{
		Bookshop book2= new Bookshop( "xyz",17,99);
		Bookshop book3= new Bookshop( "vishnu",174,9999);
		Bookshop book4= new Bookshop( "sr",117,900009);
		
		book2.buy();
		book3.buy();
		book4.buy();
		
	}
	public void buy()
	{
		System.out.println(price);
	}
}

// called  automatically when object is create
//no return data type required
// constructor should have same class name
//initializing object specific information
public Bookshop(String auther,int price,int pages)
	{
	    this.auther=auther;   // current object
	    this.price=price;
	    this.pages=pages;
}