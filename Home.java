public class Home
{
	//static refer to class specifi information in class,(global variable)
	static String name1="vishnu";
	static  int age1=20;
	//non static refers to instances(object) specific information
	//default values only global varible 
	String name2;
	int age2;
	boolean student;
	float weight;
	char lastname;
	
public static void main(String[] args)
{
	//methods
	/*int no=17;//inside method or block -local variable
System.out.println("Welcome to Java");
System.out.println(Home.name1);
System.out.println(Home.age1);
System.out.println(no);	*/
	//To create the object ,inside the method
	Home person1=new Home();//object(instance)
	System.out.println("person1 name = "+person1.name2);
	System.out.println("person1 age => "+person1.age2);
	System.out.println("person1 student => "+person1.student);
	System.out.println("person1 weight => "+person1.weight);
	System.out.println("person1 lastnme => "+person1.lastname);
/*System.out.println(person.name2);
System.out.println(person.age2);*/
}
}
