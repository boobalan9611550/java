 
//dynamic binding or late binding

public class DBchild extends DBparent
{ 
public static void main (String[]args)
{
    DBparent db = new DBchild();
	db.study();
	db.write();
	 //db.play();  can not call method 
	db.advice();
}
  void study()
 {
 System.out.println("well  child study");
 }
 void  write()
 {
 System.out.println("well child write");
 }
 void play()
 {
 System.out.println("well  child play");
 }
 
 }
 
