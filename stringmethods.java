package string;

public class stringmethods {
	public static void main(String[] args) {
		String name = "kalidass";
		String name3 = "kalidass";

		System.out.println(name.charAt(0));
		System.out.println(name.compareTo(name3));
		System.out.println(name.compareToIgnoreCase(name3));
		System.out.println(name.contains("kalidass"));
		System.out.println(name.equals(name3));

		String name4 = "boobalan";
		String name2 = new String("boobalan");
		System.out.println("=======================");
		System.out.println(name2 == name4);
		System.out.println(name2.equals(name4));
		
		
		System.out.println("========================");
		System.out.println(name2.equalsIgnoreCase(name4));
		System.out.println(name2.hashCode());
		System.out.println(name2.startsWith("bo"));
		System.out.println(name2.endsWith("n"));
		System.out.println(name4.isEmpty());
		System.out.println(name2.startsWith("bo"));
		System.out.println(name4.indexOf("o"));
		System.out.println(name2.lastIndexOf("n"));
		
		System.out.println("========================");
		
		
		String name5="  Rakesh   ";
		System.out.println(name5.substring(6));
		  System.out.println(name5.substring(0,6));
		  
		  char[] ch = name5.toCharArray();
		  for(int i=0;i<ch.length;i++) {
		   System.out.println(ch[i]);
		  }
		  System.out.println(name5.toLowerCase());
		  System.out.println(name5);
		  System.out.println(name5.toUpperCase());
		  System.out.println(name5.trim());
		  System.out.println(name5.strip());
		  System.out.println(name5);
		  System.out.println(name5.stripLeading());
		  System.out.println(name5.stripTrailing());
		  
		  String date = "11/jan/24";
		  String[] ar = date.split("/");
		  for(int i=0;i<ar.length;i++) {
		   System.out.println(ar[i]);
		  }
		  System.out.println(String.join("-", ar));
		  System.out.println(String.join("-", "11","01","2024"));
		System.out.println();
		
		
	}

}
