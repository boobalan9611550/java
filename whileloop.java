 public class whileloop
{
public static void main (String args[])
{
int count=1;
while (count<=10)
{
System.out.println(count+"");
count=count+1;
}
}
}*/

// output
// 1 2 3 4 5 6 7 8 9 10



 public class whileloop
{
public static void main (String args[])
{
int count=3;
while (count<=15)
{
System.out.println(count+"");
count=count+3;
}
}
}

// output 
//  3 6 9 12 15


 public class whileloop3 
{
public static void main (String args[])
{
int count=2;
while (count<=10)
{
System.out.println(count+"");
count=count+2;
}
}
}

// output 
// 2 4 6 8 10  
