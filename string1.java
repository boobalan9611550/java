package learnstring;

public class string1 {
	public static void main(String[] args) {
	//	String str = "abc";
	//	str="GHI";
		
		
		String state1 = "TN";
		String state2 = "TN";
		String state3 = "TN";
				
      System.out.println(state1.hashCode());   // hashcode means memory location  ex 2682
      System.out.println(state2.hashCode());         // string is immutable
      System.out.println(state3.hashCode());
      
      state1 ="AP";   
      
      System.out.println(state1.hashCode());    // memory location change ex 2095 
      System.out.println(state2.hashCode());     
      System.out.println(state3.hashCode());
      
      String name = "India";                      //literal  
      String name2 =  new String("India");         // new keyword used 
      System.out.println(name.hashCode());
      System.out.println(name2.hashCode());
      System.out.println(System.identityHashCode(name));
      System.out.println(System.identityHashCode(name2));
      
      String name3 = "BOOBALAN";
      System.out.println(name3.length());  //string length
     // int index =0;
     // while(index<8) {
    	//  System.out.println(name3.charAt(index));
    	//  index++;
    //  }
      System.out.println(name3.charAt(0));
      System.out.println(name3.charAt(1));
      System.out.println(name3.charAt(2));
      System.out.println(name3.charAt(3));
      System.out.println(name3.charAt(4));
      System.out.println(name3.charAt(5));
      System.out.println(name3.charAt(6));
      System.out.println(name3.charAt(7));
      System.out.println(name3);
      
      
	}

}
