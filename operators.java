package operators;

public class operators {
	
	 public static void main(String[] args) {
	  System.out.println(10+20);//add
	  System.out.println("vishnu"+20);// Concatenation
	  System.out.println(-10-20);//-30
	  System.out.println(10/20);//Quotient 10
	  System.out.println(10%20); //Remainder 0 if odd it 1
	  
	  //post unary/increment  operator
	  int age=20;
	  age++;//age=age+1
	  System.out.println(age); //21
	  System.out.println(age++);// First prints 21 and then increment 22
	  System.out.println(age);// 22
	  System.out.println("post unary/increment  operator");
	  //post unary/increment  operator
	    int age1=20;
	    age1--;//age=age+1
	    System.out.println(age1); //19
	    System.out.println(age1++);// First prints 19 and then decrement 18
	    System.out.println(age1);// 18
	    
	    //pre unary operator
	    int n1=10;
	     
	     System.out.println(--n1);  //9
	     System.out.println(n1);      //9
	     
	     int n2=5;
	     
	     System.out.println(++n2);  //9
	     System.out.println(n2);
	     
	     
	     int n3=6;
	    //  n3+=2;
	      System.out.println(2+n3);  //8
	      
	                                     //bitwise operator
		     System.out.println(3|2); // or operator 
		     System.out.println(3&2);  // and operator
		     System.out.println(2^3);  // exclusive or  xor
		     
		     System.out.println(2>>4);  //right shift operator   // shift operator
		     System.out.println(2<<4);   //left  shift operator
		     System.out.println(0x2222);
		       
	 }

	}


