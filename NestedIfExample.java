public class NestedIfExample {
    public static void main(String[] args) {
        int x = 10;
        int y = 5;

        if (x < 15) {
            System.out.println("Outer if");

            if (y > 27) {
                System.out.println("Inner if: Both conditions are true");
            } else {
                System.out.println("Inner if: Second condition is false");
            }
        } else {
            System.out.println("Outer if: First condition is false");
        }
    }
}