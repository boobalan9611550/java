package multithreading;

public class learnthread {

	public static void main(String[] args) {

		learnchild tc = new learnchild();
		tc.start();

		// methods

		System.out.println(tc.getState());  //runnable
		System.out.println(tc.getId());      //13
		System.out.println(tc.getName());    //thread-0
		System.out.println(tc.getPriority()); //5
		System.out.println(tc.isAlive());     //true
		System.out.println(tc.isDaemon());    //false 		
		System.out.println(tc.MIN_PRIORITY);   //1
		System.out.println(tc.MAX_PRIORITY);   //10
		
	  //  tc.interrupted();              //stop the thread and return start
	    
		System.out.println(tc.getName());
		tc.setName("vishnu");
		System.out.println(tc.getName());
			
	    System.out.println(tc.getPriority());
	    tc.setPriority(7);
	    System.out.println(tc.getPriority());
	    
	    
		
		for (int no = 1; no <= 5; no++) {
			System.out.println("TreadDemo " + no);
			System.out.println(tc.getState());

		}
		System.out.println(tc.getState());

	}

}
